import React, { useEffect, useState } from 'react';
import { io } from 'socket.io-client';

import BandAdd from './components/BandAdd';
import BandList from './components/BandList';

const connectSocketServer = () => {
  const socket = io.connect('http://localhost:8080');
  return socket;
};

const App = () => {
  const [socket] = useState(connectSocketServer());
  const [online, setOnline] = useState(false);
  const [bands, setBands] = useState([]);

  useEffect(() => {
    setOnline(socket.connected);
  }, [socket]);

  useEffect(() => {
    socket.on('connect', () => {
      setOnline(true);
    });
  }, [socket]);

  useEffect(() => {
    socket.on('disconnect', () => {
      setOnline(false);
    });
  }, [socket]);

  useEffect(() => {
    socket.on('current-bands', (bands) => {
      setBands(bands);
    });
  }, [socket]);

  const votar = (id) => {
    socket.emit('votar-banda', id);
  };

  const deleteBand = (id) => {
    socket.emit('delete-band', id);
  };

  const cambiarNameBand = (id, name) => {
    socket.emit('newName-band', { id, name });
  };

  const newBand = (name) => {
    socket.emit('addNew-band', name);
  };
  return (
    <div className='container'>
      <div className='alert'>
        <p>
          Service status:
          {online ? (
            <span className='text-success'>Online</span>
          ) : (
            <span className='text-danger'>Offline</span>
          )}
        </p>
      </div>
      <h1>BandNames</h1>
      <hr />
      <div className='row'>
        <div className='col-8'>
          <BandList
            bands={bands}
            votar={votar}
            deleteBand={deleteBand}
            cambiarNameBand={cambiarNameBand}
          />
        </div>
        <div className='col-4'>
          <BandAdd newBand={newBand} />
        </div>
      </div>
    </div>
  );
};

export default App;
