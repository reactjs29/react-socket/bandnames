import React, { useEffect, useState } from 'react';

const BandList = ({ bands, votar, deleteBand, cambiarNameBand }) => {
  const [band, setBand] = useState(bands);

  useEffect(() => {
    setBand(bands);
  }, [bands]);

  const cambioNombre = (event, id) => {
    const newName = event.target.value;
    console.log(newName);
    // setBand((band) =>
    //   band.map((item) => {
    //     if (item.id === id) {
    //       item.name = newName;
    //     }
    //     return item;
    //   })
    // );
    setBand((bands) =>
      bands.map((band) => {
        if (band.id === id) {
          band.name = newName;
        }
        return band;
      })
    );
  };

  const perdioFoco = (name, id) => {
    cambiarNameBand(id, name);
  };

  const crearRows = () => {
    return band.map(({ votes, name, id }) => (
      <tr key={id}>
        <td>
          <button className='btn btn-primary' onClick={() => votar(id)}>
            +1
          </button>
        </td>
        <td>
          <input
            className='form-control'
            value={name}
            onChange={(event) => cambioNombre(event, id)}
            onBlur={() => perdioFoco(name, id)}
          />
        </td>
        <td>
          <h4>{votes}</h4>
        </td>
        <td>
          <button className='btn btn-danger' onClick={() => deleteBand(id)}>
            <i className='fa-solid fa-trash-can'></i>
          </button>
        </td>
      </tr>
    ));
  };
  return (
    <>
      <table className='table table-stripped'>
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Votes</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>{crearRows()}</tbody>
      </table>
    </>
  );
};

export default BandList;
