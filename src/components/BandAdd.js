import React, { useState } from 'react';

const BandAdd = ({ newBand }) => {
  const [nameBand, setNameBand] = useState('');

  const onSubmit = (e) => {
    e.preventDefault();
    if (nameBand.trim().length > 0) {
      newBand({ name: nameBand });
      setNameBand('');
    }
  };
  return (
    <>
      <h3>Agregar Banda</h3>
      <form onSubmit={onSubmit}>
        <input
          className='form-control'
          placeholder='Nombre de banda'
          value={nameBand}
          onChange={(event) => setNameBand(event.target.value)}
        />
      </form>
    </>
  );
};

export default BandAdd;
